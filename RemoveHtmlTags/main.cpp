#include <iostream>
#include <fstream>
#include <string>

extern "C"
{
    unsigned int __stdcall removeHtmlTag(char*, char*);
}

int main(int argc, char* argv[])
{
    if (argc >= 3)
    {
        std::ifstream fin(argv[1]);

        if (!fin.is_open())
        {
            std::cerr
                << std::string("Cannot open file ") + argv[1]
                << std::endl;
            exit(EXIT_FAILURE);
        }

        std::string buffer(
            (std::istreambuf_iterator<char>(fin)),
            std::istreambuf_iterator<char>()
            );
        int textLength = buffer.length();

        if (textLength == 0)
        {
            std::cerr
                << "File is empty"
                << std::endl;
            exit(EXIT_FAILURE);
        }

        char* fileData = new char[textLength + 1];
        std::copy(buffer.begin(), buffer.end(), fileData);
        fileData[textLength] = '\0';

        for (int i = 2; i < argc; i++)
        {
            removeHtmlTag(fileData, argv[i]);
        }

        fin.close();

        std::ofstream fout(argv[1]);
        fout << fileData;
        if (!fout)
        {
            std::cerr
                << std::string("Cannot write to file") + argv[1]
                << std::endl;
        }
        fout.close();

        delete[] fileData;
    }
    else
    {
        std::cerr
            << "Usage: RemoveHtmlTags.exe index.html p div"
            << std::endl;
        exit(EXIT_FAILURE);
    }

    return 0;
}