format MS COFF

public removeHtmlTag as '_removeHtmlTag@8'

extrn '__imp__strlen' as strlen: dword
extrn '__imp__memset' as memset: dword
extrn '__imp___strcmpi' as strcmpi: dword

include 'include\win32a.inc'

section '.data' data readable writeable

    indexStartTag           dd              0
    indexEndTag             dd              0
    indexFirstTag           dd              0

    countOpenTag            db              0

    inClosedTag             db              0

    searchOpenArrow         db              0
    isOpenArrowFound        db              0
    searchCloseArrow        db              0

    searchCloseComment      db              0

    searchTagName           db              0
    isTagNameFound          db              0

    searchCloseQuote        db              0

    cmpTag                  db              32 dup(0)

section '.text' code readable executable

proc removeHtmlTag text: dword, tagName: dword

    xor                     eax,            eax
    xor                     ebx,            ebx
    xor                     ecx,            ecx
    xor                     edx,            edx

    mov                     edx,            cmpTag
    mov                     esi,            [text]
    mov                     [searchOpenArrow],      1


.main:

    lodsb

    cmp                     al,                     0
    je                      .exit

    cmp                     [searchOpenArrow],      1
    je                      .lSearchOpenArrow

    cmp                     [searchCloseArrow],     1
    je                      .lSearchCloseArrow

    cmp                     [searchCloseComment],   1
    je                      .lSearchCloseComment

    cmp                     [searchTagName],        1
    je                      .lSearchTagName

    cmp                     [searchCloseQuote],     1
    je                      .lSearchCloseQuote

; Open tag.

.lSearchOpenArrow:

    cmp                     al,             '"'; 22
    je                      .quoteFound

    cmp                     al,             '<'; 3C
    jne                     .main

    lodsb

    cmp                     al,             '!'; 21
    jne                     .decEsiOpenArrow

    lodsb

    cmp                     al,             '-'; 2D
    jne                     .subEsi2OpenArrow

    lodsb

    cmp                     al,             '-'; 2D
    je                      .commentFound

    sub                     esi,                3

    jmp                     .openArrowFound


.decEsiOpenArrow:

    dec                     esi

    jmp                     .openArrowFound


.subEsi2OpenArrow:

    sub                     esi,                2

    jmp                     .openArrowFound


.openArrowFound:

    mov                     al,                 byte [esi - 1]

    mov                     [inClosedTag],      0

    mov                     [indexStartTag],    esi
    dec                     [indexStartTag]

    cmp                     [countOpenTag],     0
    jne                     .openArrowFoundCon

    mov                     [indexFirstTag],    esi
    dec                     [indexFirstTag]

    jmp                     .openArrowFoundCon


.openArrowFoundCon:

    mov                     [searchOpenArrow],  0
    mov                     [isOpenArrowFound], 1
    mov                     [searchTagName],    1

    jmp                     .main

; Tags comparasion.

.lSearchTagName:

    cmp                     al,                 '/';  2F
    jne                     .lSearchTagNameCon

    mov                     [inClosedTag],      1

    jmp                     .main

.lSearchTagNameCon:

    cmp                     al,                 '<'; 3C
    jne                     .checkTag

    lodsb

    cmp                     al,                 '!'; 21
    jne                     .decEsiTagName

    lodsb

    cmp                     al,                 '-'; 2D
    jne                     .subEsi2TagName

    lodsb

    cmp                     al,                 '-'; 2D
    je                      .commentFound

    sub                     esi,                3

    jmp                     .movAlEsiTagName


.decEsiTagName:

    dec                     esi

    jmp                     .movAlEsiTagName


.subEsi2TagName:

    sub                     esi,                2

    jmp                     .movAlEsiTagName


.movAlEsiTagName:

    mov                     al,                 byte [esi - 1]

    jmp                     .checkTag


.checkTag:

    cmp                     al,                 ' '; 20
    je                      .cmpTags

    cmp                     al,                  9; tab
    je                      .cmpTags

    cmp                     al,                  10; new line
    je                      .cmpTags

    cmp                     al,                  13; carriage return
    je                      .cmpTags


    cmp                     al,                 '>'; 3E
    je                      .decEsiCmpTags

    mov                     [edx],              al

    inc                     edx

    jmp                     .main


.decEsiCmpTags:

    dec                     esi

    mov                     al,                 byte [esi - 1]

    jmp                     .cmpTags


.cmpTags:

    ; eax == 0 - tags are equal.
    ccall [strcmpi], [tagName], cmpTag

    cmp                     eax,                0
    je                      .tagFound

    ; else

    mov                     edx,                cmpTag

    push                    eax
    push                    ecx
    push                    edx

    ccall [memset],         edx, 0, 32
    mov                     edx,                cmpTag

    pop                     edx
    pop                     ecx
    pop                     eax;                xor eax, eax

    mov                     [searchOpenArrow],  1
    mov                     [searchTagName],    0

    jmp                     .main

.tagFound:

    push                    eax
    mov                     edx,                cmpTag
    ccall [memset],         edx, 0, 32
    mov                     edx,                cmpTag
    pop                     eax

    cmp                     [inClosedTag],      1
    je                      .tagFoundCon

    inc                     [countOpenTag];

    jmp                     .tagFoundCon


.tagFoundCon:

    cmp                     [inClosedTag],          1
    jne                     .tagFoundCon2

    mov                     [searchOpenArrow],      1
    mov                     [searchTagName],        0

    dec                     [countOpenTag]

    cmp                     [countOpenTag],         0
    jne                     .main

    mov                     [searchCloseArrow],     1
    mov                     [searchOpenArrow],      0

    jmp                     .tagFoundCon2


.tagFoundCon2:

    mov                     [searchCloseArrow],     1
    mov                     [searchTagName],        0
    mov                     [isTagNameFound],       1

    push                    eax
    mov                     edx,                    cmpTag
    ccall [memset],         edx, 0, 32
    mov                     edx,                    cmpTag
    pop                     eax

    jmp                     .main

; Close tag.

.lSearchCloseArrow:

    cmp                     al,                     '"'; 22
    je                      .quoteFound

    cmp                     al,                     '<'; 3C
    jne                     .chekIfItCloseArrow

    lodsb

    cmp                     al,                     '!'; 21
    jne                     .decEsiCloseArrow

    lodsb

    cmp                     al,                     '-'; 2D
    jne                     .subEsi2CloseArrow

    lodsb

    cmp                     al,                     '-'; 2D
    je                      .commentFound

    sub                     esi,                    3

    jmp                     .chekIfItCloseArrow

.decEsiCloseArrow:

    dec                     esi

    jmp                     .movAlEsiCloseArrow


.subEsi2CloseArrow:

    sub                     esi,                    2

    jmp                     .movAlEsiCloseArrow


.movAlEsiCloseArrow:

    dec                     esi
    mov                     al,                     byte [esi - 1]

    jmp                     .chekIfItCloseArrow


.chekIfItCloseArrow:

    cmp                     al,                     '>'; 3E
    jne                     .main

    mov                     [indexEndTag],          esi

    mov                     [searchOpenArrow],      1
    mov                     [isTagNameFound],       0
    mov                     [searchCloseArrow],     0
    mov                     [isOpenArrowFound],     0

    jmp                     .removeTag

; Attributes.

.quoteFound:

    mov                     [searchCloseArrow],     0
    mov                     [searchOpenArrow],      0
    mov                     [searchCloseQuote],     1

    jmp                     .main

.lSearchCloseQuote:

    cmp                     al,                     '"'; 22
    jne                     .main

    ; Close quote is found.

    mov                     [searchCloseArrow],     1
    mov                     [searchCloseQuote],     0

    cmp                     [isTagNameFound],       1
    je                      .main

    mov                     [searchCloseArrow],     0
    mov                     [searchOpenArrow],      1

    jmp                     .main

; Comments.

.commentFound:

    mov                     [searchOpenArrow],      0
    mov                     [searchCloseArrow],     0
    mov                     [searchTagName],        0
    mov                     [searchCloseComment],   1

    jmp                     .main


.lSearchCloseComment:

    cmp                     al,                     '-'; 2D
    jne                     .main

    lodsb

    cmp                     al,                     '-'
    jne                     .main

    lodsb

    cmp                     al,                     '>'; 3E
    jne                     .main

    mov                     [searchCloseComment],   0
    mov                     [searchOpenArrow],      1

    cmp                     [isOpenArrowFound],     1
    je                      .conSearchCloseArrowOrTagName

    jmp                     .main


.conSearchCloseArrowOrTagName:

    mov                     [searchOpenArrow],      0
    mov                     [searchCloseArrow],     1

    cmp                     [isTagNameFound],       1
    je                      .main

    mov                     [searchCloseArrow],     0
    mov                     [searchTagName],        1

    jmp                     .main

; Removing.

.removeTag:

    mov                     esi,                    [indexEndTag]
    mov                     edi,                    [indexStartTag]

    cmp                     [countOpenTag],         0
    jne                     .removeTagCon

    mov                     edi,                    [indexFirstTag]

    jmp                     .removeTagCon


.removeTagCon:

    push                    edi

    ccall                   [strlen],               esi
    mov                     ecx,                    eax

    rep                     movsb

    mov                     eax,                    0
    pop                     esi
    mov                     [edi],                  al

    mov                     edx,                    cmpTag

    push                    eax
    push                    ecx
    push                    edx

    ccall [memset],         edx, 0, 32

    pop                     edx
    pop                     ecx
    pop                     eax

    jmp                     .main


.exit:
    ret
endp
